#SCHOOL BUS TRACKER

Used for tracking students school bus, it is an IOT device fully controlled by the web.
Its internal component are:
*ATMEGA microcontroller(**atmel** ATMEGA2560)
*GPS receiver(**UBLOX** NEON-6M)
*NFC reader/writer(**NXP** PN532)
*GSM transimitter/receiver(**SIMCOM** SIM800L)
*RTC clock(**MAXIM** DS3231)
*GLCD graphical display(**SITRONIX** ST7920)

-It reads `NDEF` fromatted cards and writes on them.
-`MQTT` protocol is used to communicate with the server.
-Transmitted data are formatted in `JSON`.
```json
{
  "deviceID": "DV2560A0001",
  "gpsData": [
	   -1.944591,
		30.089494,
		0.40744
	],
	"cardID": "",
	"student#": 0
}

```
-Also data received are formatted id `JSON`.
```json
not yet defined :(.
```
